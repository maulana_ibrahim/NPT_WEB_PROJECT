package NPT;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Maulana
 */
public class NPTServlet extends HttpServlet {

    private static NPT.MySQLLoader mysqlLoader = new NPT.MySQLLoader();
    private static NPT.MsSQLLoader mssqlLoader = new NPT.MsSQLLoader();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
  

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<String> valueCsvCol = new ArrayList<>();
        ArrayList<String> typeColumn = new ArrayList<>();
        ArrayList<String> nameColumn = new ArrayList<>();
        ArrayList<String> listvalueColGap = new ArrayList<>();
        ArrayList<String> listvalueGap = new ArrayList<>();
        ArrayList<HashMap<String, String>> rows = new ArrayList<>();
        String nameColumnCreatTable = null;
        Scanner scanner = new Scanner(new File("G:\\tugas kuliah\\tingkat 2\\Semester 4\\CV\\kerja_praktek\\csv_sample_data.csv"));

        while (scanner.hasNext()) {
            valueCsvCol.add(scanner.nextLine());
        }
        
        for (int i = 0; i < valueCsvCol.size(); i++) {
            System.out.println("Row Csv ke : " + (i + 1));
            String[] Hasil = valueCsvCol.get(i).split(",");
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String H_Date_yesterday = df.format(new Date(new Date().getTime()));
            final String H_Date = df.format(new Date(new Date().getTime() - (24 * 60 * 60 * 1000)));
//            String H_1_Date = df.format(new Date(new Date().getTime() - (2 * 24 * 60 * 60 * 1000)));
            String H_1_Date = df.format(new Date(new Date().getTime() - (24 * 60 * 60 * 1000)));
            String NPT_COUNTER_DATE = df.format(new Date(new Date().getTime()));
            final String dbName = Hasil[1];
            final String tableName = Hasil[4];
            final String query = Hasil[3];

            String tableQuery = dbName + "_" + tableName;
            String tableQueryGap = dbName + "_" + tableName + "_DAILY_CHANGE";
            String tableQueryNPT = dbName + "_" + tableName + "_NPT";
            typeColumn = mysqlLoader.getTypeColumn(tableName, dbName);
            nameColumn = mysqlLoader.getNameColumn(tableName, dbName);
            nameColumnCreatTable = StringUtils.join(typeColumn, ",");
            
            try {
                rows = mysqlLoader.loadYesterdayDataFromStylo(nameColumn, query, H_Date_yesterday);
                try {
                    if (mssqlLoader.tabelExist(tableQuery).equals("1")) {
                        if (mssqlLoader.checkAmounColumnTableStylo(nameColumn, tableName, dbName).equals("equals")) {

                            InsertionToStyloGapTableAndGenerateCSV(tableQueryGap, nameColumn, query, H_1_Date, rows, tableName, dbName, listvalueColGap, listvalueGap, nameColumnCreatTable, H_Date_yesterday, tableQueryNPT);

                            GenerateCSVNPTCompare(nameColumn, query, rows, tableQueryNPT, H_Date_yesterday);
                        } else if (mssqlLoader.checkAmounColumnTableStylo(nameColumn, tableName, dbName).equals("notequals")) {
                            if (mssqlLoader.AlterTabletoStylo(typeColumn, nameColumn, dbName, tableName).equals("sukses")) {

                                InsertionToStyloGapTableAndGenerateCSV(tableQueryGap, nameColumn, query, H_1_Date, rows, tableName, dbName, listvalueColGap, listvalueGap, nameColumnCreatTable, H_Date_yesterday, tableQueryNPT);
//
                                GenerateCSVNPTCompare(nameColumn, query, rows, tableQueryNPT, H_Date_yesterday);
                            } else {
                                System.err.println("gagal alter table");
                            }
                        }
                    } else {
                        if (mssqlLoader.createtable(tableQuery, nameColumnCreatTable).equals("sukses")) {

                            InsertionToStyloGapTableAndGenerateCSV(tableQueryGap, nameColumn, query, H_1_Date, rows, tableName, dbName, listvalueColGap, listvalueGap, nameColumnCreatTable, H_Date_yesterday, tableQueryNPT);
//
                            GenerateCSVNPTCompare(nameColumn, query, rows, tableQueryNPT, H_Date_yesterday);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        request.setAttribute("namacolumn", nameColumnCreatTable);
        RequestDispatcher rd=request.getRequestDispatcher("/ListCsv.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public static void GenerateCSVNPTCompare(final ArrayList<String> nameColumn, final String query, final ArrayList<HashMap<String, String>> rows, final String tableQueryNPT, final String H_Date_yesterday) {

        try {
            ArrayList<HashMap<String, String>> NPTDeltaValue = mssqlLoader.compareDataNPT(nameColumn, query, rows, tableQueryNPT, "2018-08-09");
            mssqlLoader.generateCsv(tableQueryNPT, NPTDeltaValue, nameColumn, H_Date_yesterday);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void InsertionToStyloGapTableAndGenerateCSV(final String tableQueryGap, final ArrayList<String> nameColumn, final String query, final String H_1_Date, final ArrayList<HashMap<String, String>> rows, final String tableName, final String dbName, final ArrayList<String> listvalueColGap, final ArrayList<String> listvalueGap, final String nameColumnCreatTable, final String H_Date_yesterday, final String tableQueryNPT) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
        try {
            if (mssqlLoader.tabelExist(tableQueryGap).equals("1")) {
                ArrayList<HashMap<String, String>> changeDate = mssqlLoader.insertionToStyloGapTable(nameColumn, query, H_1_Date, rows, tableName, dbName, listvalueColGap, listvalueGap, nameColumnCreatTable, tableQueryGap);
                mssqlLoader.generateCsv(tableQueryGap, changeDate, nameColumn, H_Date_yesterday);
            } else {
                if (mssqlLoader.createtable(tableQueryNPT, nameColumnCreatTable).equals("sukses")) {
                    ArrayList<HashMap<String, String>> changeDate = mssqlLoader.insertionToStyloGapTable(nameColumn, query, H_1_Date, rows, tableName, dbName, listvalueColGap, listvalueGap, nameColumnCreatTable, tableQueryGap);
                    mssqlLoader.generateCsv(tableQueryGap, changeDate, nameColumn, H_Date_yesterday);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//            }
//        }).start();
    }

}
