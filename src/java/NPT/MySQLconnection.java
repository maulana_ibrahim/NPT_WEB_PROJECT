/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NPT;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Maulana
 */
public class MySQLconnection {
    //Connection connection = null;

    static Connection connection = null;

    public Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if (connection == null) {
                connection = (Connection) DriverManager.getConnection("jdbc:mysql://10.2.124.195/STYLO_E_2GR?autoReconnect=true&useUnicode=yes&user=dbstylo&password=stylo2018#");
            }

        } catch (ClassNotFoundException e) {

            e.printStackTrace();

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return connection;
    }

    public void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            //do nothing
        }
    }

}
