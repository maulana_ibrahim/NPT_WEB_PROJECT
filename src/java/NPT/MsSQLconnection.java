package NPT;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author Maulana
 */
public class MsSQLconnection {

    static Connection connectionMSSQL = null;

    public Connection getConnection() throws SQLException {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            if (connectionMSSQL == null) {
                connectionMSSQL = (Connection) DriverManager.getConnection("jdbc:sqlserver://10.54.18.212;"
                        + "databaseName=NPT2;user=united;password=Goal1;");
//                    connectionMSSQL = (Connection) DriverManager.getConnection("jdbc:sqlserver://DESKTOP-00U9NGH\\SQLEXPRESS;databaseName=NPT2;user=sa;password=maulana;");
            }
//                

        } catch (ClassNotFoundException e) {

            e.printStackTrace();

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return connectionMSSQL;
    }

    public void closeConnection() {
        try {
            if (connectionMSSQL != null) {
                connectionMSSQL.close();
            }
        } catch (SQLException e) {
            //do nothing
        }
    }
}
