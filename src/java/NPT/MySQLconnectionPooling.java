/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NPT;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Maulana
 */
public class MySQLconnectionPooling {
    private List<Connection> availableConnections = new ArrayList<Connection>();
    private List<Connection> usedConnections = new ArrayList<Connection>();
    private final int MAX_CONNECTIONS = 10;

    private String URL;
    private String USERID;
    private String PASSWORD;

    public MySQLconnectionPooling(String Url, String UserId, String password)
            throws SQLException {
        this.URL = Url;
        this.USERID = UserId;
        this.PASSWORD = password;

        for (int count = 0; count < MAX_CONNECTIONS; count++) {
            availableConnections.add(this.createConnection());
        }

    }

    public Connection createConnection() throws SQLException {
        return DriverManager.getConnection(this.URL, this.USERID, this.PASSWORD);
    }

    public Connection getConnection() {
        if (availableConnections.size() == 0) {
            System.out.println("All connections are Used !!");
            return null;
        } else {
            Connection con
                    = availableConnections.remove(
                            availableConnections.size() - 1);
            usedConnections.add(con);
            return con;
        }
    }

    public boolean releaseConnection(Connection con) {
        if (null != con) {
            usedConnections.remove(con);
            availableConnections.add(con);
            return true;
        }
        return false;
    }

    public int getFreeConnectionCount() {
        return availableConnections.size();
    }
}
