/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NPT;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Maulana
 */
public class MySQLLoader {

    private static MySQLconnection conectionMysql = new MySQLconnection();

    public static ArrayList<String> getTypeColumn(String tableName, String dbName) {
        ArrayList<String> typeColumn = new ArrayList<>();
        String dataType;
        try {

            Statement statement = conectionMysql.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT COLUMN_NAME,COLUMN_TYPE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION "
                    + "FROM INFORMATION_SCHEMA.COLUMNS "
                    + "where table_name='" + tableName + "' and TABLE_SCHEMA='" + dbName + "'");
            while (resultSet.next()) {
                if (resultSet.getString("DATA_TYPE").equals("int") || resultSet.getString("DATA_TYPE").equals("INT")) {
                    dataType = "BIGINT";
                } else if (resultSet.getInt("CHARACTER_MAXIMUM_LENGTH") > 8000) {
                    dataType = "VARCHAR (MAX)";
                } else if (resultSet.getString("DATA_TYPE").equals("mediumtext") || resultSet.getString("DATA_TYPE").equals("MEDIUMTEXT")) {
                    dataType = "nvarchar (" + resultSet.getString("CHARACTER_MAXIMUM_LENGTH") + ")";
                } else if (resultSet.getString("DATA_TYPE").equals("varchar") || resultSet.getString("DATA_TYPE").equals("VARCHAR")) {
                    dataType = "VARCHAR (" + resultSet.getString("CHARACTER_MAXIMUM_LENGTH") + ")";
                } else if (resultSet.getString("DATA_TYPE").equals("datetime") || resultSet.getString("DATA_TYPE").equals("DATETIME")) {
                    dataType = "datetime2";
                } else if (resultSet.getString("DATA_TYPE").equals("timestamp") || resultSet.getString("DATA_TYPE").equals("TIMESTAMP")) {
                    dataType = "smalldatetime";
                } else if (resultSet.getString("DATA_TYPE").equals("double") || resultSet.getString("DATA_TYPE").equals("DOUBLE")) {
                    dataType = "float";
                } else {
                    dataType = resultSet.getString("DATA_TYPE");
                }
                typeColumn.add(resultSet.getString("COLUMN_NAME") + " " + dataType);
            }
            //statement.close();

        } catch (SQLException e) {
            e.getErrorCode();
        }
        return typeColumn;
    }

    public static ArrayList<String> getNameColumn(String tableName, String dbName) {
        ArrayList<String> nameColumn = new ArrayList<>();
        try {
            Statement statement = conectionMysql.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT COLUMN_NAME,COLUMN_TYPE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION "
                    + "FROM INFORMATION_SCHEMA.COLUMNS "
                    + "where table_name='" + tableName + "' and TABLE_SCHEMA='" + dbName + "'");
            while (resultSet.next()) {
                nameColumn.add(resultSet.getString("COLUMN_NAME"));
            }
            //statement.close();
        } catch (SQLException e) {
        }
        
        return nameColumn;
    }
    
    
    public static ArrayList<HashMap<String, String>> load2LastDayDataFromStylo(List<String> listNameColumn, String query, String H_1_Date) throws SQLException {
        Statement statement = conectionMysql.getConnection().createStatement();
        ResultSet curRs = statement.executeQuery(query + "'" + H_1_Date + "'");
        int counter = 0;
        ArrayList<HashMap<String, String>> rows = new ArrayList<>();
        try {
            while (curRs.next()) {
//            if (counter < 5) {
                HashMap<String, String> row = new LinkedHashMap();
                for (int i = 0; i < listNameColumn.size(); i++) {
                    row.put(listNameColumn.get(i), curRs.getString(listNameColumn.get(i)));
                }
                rows.add(row);
//                counter++;
//            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            statement.close();
        }
        return rows;
    }
    
    
    public static ArrayList<HashMap<String, String>> loadYesterdayDataFromStylo(ArrayList<String> listNameColumn, String query, String H_Date) throws SQLException {
//       Connection conectionMYSQL = pool.getConnection();
//        if (conectionMYSQL.equals(null)) {
//            conectionMYSQL = pool.createConnection();
//        } else {
//            conectionMYSQL=conectionMYSQL;
//        }
//       Statement statement = conectionMYSQL.createStatement(); 
        Statement statement = conectionMysql.getConnection().createStatement();
        ResultSet curRs = statement.executeQuery(query + "'" + H_Date + "'");
        int counter = 0;
        ArrayList<HashMap<String, String>> rows = new ArrayList<>();
        try {
            while (curRs.next()) {
//            if (counter < 5) {
                HashMap<String, String> row = new LinkedHashMap();
                for (int i = 0; i < listNameColumn.size(); i++) {
                    row.put(listNameColumn.get(i), curRs.getString(listNameColumn.get(i)));
                }
                rows.add(row);
//                counter++;
//            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            statement.close();
        }
        return rows;
    }

    
    
    

}
