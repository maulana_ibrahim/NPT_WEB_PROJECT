package NPT;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Maulana
 */
public class MsSQLLoader {

    private static MsSQLconnection conectionMsSQL = new MsSQLconnection();
    private static MySQLconnection conectionMySQL = new MySQLconnection();
    private static MySQLLoader mysqlLoader = new MySQLLoader();
    private static CsvFileWriter csvWriter = new CsvFileWriter();
    private static Statement statement;
    private static Connection con;

    public static String createtable(String tabel, String namaColumn) throws SQLException {
        String condition = null;
        try {
            String query = "CREATE TABLE [NPT2].[dbo].[" + tabel + "] (" + namaColumn + ")";
            Statement stmt = null;
            con = conectionMsSQL.getConnection();
            stmt = con.createStatement();
            stmt.executeUpdate(query);
            System.err.println("Sukses create table");
            condition = "sukses";
        } catch (Exception e) {
            condition = "gagal";
            System.err.println("gagal creat table mssql " + tabel);
        }
        return condition;
    }

    public static String InsertionMSSQL(String tabel, String values, String namacolumn) throws SQLException {
        String x = null;
        try {
            //System.out.println("INSERT INTO [NPT2].[dbo].["+tabel+"] ("+namacolumn+") VALUES ("+values+")");
            PreparedStatement preparedStatement = conectionMsSQL.getConnection().prepareStatement("INSERT INTO [NPT2].[dbo].[" + tabel + "] (" + namacolumn + ") VALUES (" + values + ")");
            if (preparedStatement.executeUpdate() != 1) {
                x = "gagal";
            } else {
                x = "sukses";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return x;
    }

    public static String tabelExist(String tabel) throws SQLException {
        String x = null;
        try {
            Statement smt = conectionMsSQL.getConnection().createStatement();
            ResultSet resultSet = smt.executeQuery("SELECT COUNT(*) AS jmlhrow FROM NPT2.INFORMATION_SCHEMA.TABLES where TABLE_NAME = '" + tabel + "'");

            while (resultSet.next()) {
                x = resultSet.getString("jmlhrow");
            }
            resultSet.close();
            smt.close();
        } catch (Exception e) {
            System.err.println("error di tabelexist = " + tabel);
        }

        return x;
    }

    public static ArrayList<HashMap<String, String>> load2LastDayFromStylo(List<String> listNameColumn, String query, String H_1_Date, String tabel) throws SQLException {
        Statement statement = conectionMsSQL.getConnection().createStatement();
        ResultSet curRs = statement.executeQuery("SELECT * FROM [NPT2].[dbo].[" + tabel + "] WHERE DATETIME_ID='" + H_1_Date + "'");
        int counter = 0;
        ArrayList<HashMap<String, String>> rows = new ArrayList<>();
        try {
            while (curRs.next()) {
//                if (counter < 5) {
                    HashMap<String, String> row = new LinkedHashMap();
                    for (int i = 0; i < listNameColumn.size(); i++) {
                        row.put(listNameColumn.get(i), curRs.getString(listNameColumn.get(i)));
                    }
                    rows.add(row);
//                    counter++;
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows;
    }
    
    public static ArrayList<HashMap<String, String>> loadNPTDataFromStylo(List<String> listNameColumn, String NPT_DATE_COUNTER, String tabel) throws SQLException {
        Statement statement = conectionMsSQL.getConnection().createStatement();
        ResultSet curRs = statement.executeQuery("SELECT * FROM [NPT2].[dbo].[" + tabel + "] WHERE DATETIME_ID='" + NPT_DATE_COUNTER + "'");
        int counter = 0;
        ArrayList<HashMap<String, String>> rows = new ArrayList<>();
        try {
            while (curRs.next()) {
//                if (counter < 5) {
                    HashMap<String, String> row = new LinkedHashMap();
                    for (int i = 0; i < listNameColumn.size(); i++) {
                        row.put(listNameColumn.get(i), curRs.getString(listNameColumn.get(i)));
                    }
                    rows.add(row);
//                    counter++;
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rows;
    }
    
    public static ArrayList<HashMap<String, String>> loadyesterdayFromStylo(List<String> listNameColumn, String query, String H_Date, String tabel) throws SQLException {
        Statement statement = conectionMsSQL.getConnection().createStatement();
        ResultSet curRs = statement.executeQuery("SELECT * FROM [NPT2].[dbo].[" + tabel + "] WHERE DATETIME_ID='" + H_Date + "'");
        int counter = 0;
        ArrayList<HashMap<String, String>> rows = new ArrayList<>();
        while (curRs.next()) {
//            if (counter < 5) {
                HashMap<String, String> row = new LinkedHashMap();
                for (int i = 0; i < listNameColumn.size(); i++) {
                    row.put(listNameColumn.get(i), curRs.getString(listNameColumn.get(i)));
                }
                rows.add(row);
//                counter++;
//            }
        }
        return rows;
    }
    

    
    public static void insertionToStyloTable(ArrayList<HashMap<String, String>> rows, String dbName, String tableName, String tabelQuery) throws SQLException {
        ArrayList<String> resultValue = new ArrayList<>();
        ArrayList<String> resultColumn = new ArrayList<>();
        Statement stmt = conectionMsSQL.getConnection().createStatement();
        int sukses = 0;
        int gagal = 0;
        int initdata = 0;
        int limitdata = 5000;
        System.out.println(rows.size());
        try {
            for (Iterator<HashMap<String, String>> iterator = rows.iterator(); iterator.hasNext();) {
                HashMap<String, String> next = iterator.next();
                for (Map.Entry<String, String> entry : next.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    if (value == null) {
                        value = "''";
                    } else {
                        value = "'" + value + "'";
                    }
                    //resultColumn.add(key);
                    resultValue.add(value);
                }
                //String resultColumndata = StringUtils.join(resultColumn, ",");
                String resultdata = StringUtils.join(resultValue, ",");
//                    stmt.addBatch("INSERT INTO [NPT2].[dbo].[" + tabelQuery+ "] (" + resultColumndata + ") VALUES (" + resultdata + ")");
                String query = "INSERT INTO [NPT2].[dbo].[" + tabelQuery + "] VALUES (" + resultdata + ")";
                stmt.addBatch(query);
                if (initdata % 10000 == 0) {
                    System.err.println("terisnert = " +tabelQuery+"="+initdata);
                    stmt.executeBatch();
                    stmt.clearBatch();
                }
                initdata++;
                resultValue.clear();
                resultColumn.clear();
            }
            long start = System.currentTimeMillis();
            int[] updateCounts = stmt.executeBatch();
            long end = System.currentTimeMillis();
            System.out.println("total time taken to insert the batch = " + (end - start) + " ms");
            printStatusInsertionStylo(gagal, dbName, tableName, updateCounts.length, gagal);
            stmt.clearBatch();
            stmt.close();

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("error isertion tabel" + dbName + "_" + tableName);
        }

    }

    private static void printStatusInsertionStylo(int countData, final String dbName, final String tableName, int sukses, int gagal) {
        System.out.println("jumlah data yang terinsert di " + dbName + "_" + tableName + "=" + sukses);
        System.out.println("jumlah data yang gagal terinsert di " + dbName + "_" + tableName + "=" + gagal);
    }

    private static void printStatusInsertionStylogap(int countData, final String dbName, final String tableName, int sukses, int gagal) {
        System.out.println("jumlah data yang terinsert di " + dbName + "_" + tableName + "_gap =" + sukses);
        System.out.println("jumlah data yang gagal terinsert di " + dbName + "_" + tableName + "_gap =" + gagal);
    }

    public static ArrayList<HashMap<String, String>> insertionToStyloGapTable(List<String> listNameColumn, String query, String H_1_Date, ArrayList<HashMap<String, String>> rows, final String tableName, final String dbName, List<String> listvalueColGap, List<String> listvalueGap, String typecolumn, String tableQueryGap) throws SQLException {
        ArrayList<HashMap<String, String>> changesData = compareData(listNameColumn, query, H_1_Date, rows, dbName + "_" + tableName);
        System.err.println("comparation selesai");
        Statement stmt = conectionMsSQL.getConnection().createStatement();
        int countdata = 0;
        int sukses = 0;
        int gagal = 0;
        int initdata = 0;
        int limitdata = 5000;
        int[] updateCounts;
        String returncondition = null;
        try {
            for (Iterator<HashMap<String, String>> iterator = changesData.iterator(); iterator.hasNext();) {
                HashMap<String, String> next = iterator.next();
                for (Map.Entry<String, String> entry : next.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    if (value == null) {
                        value = "''";
                    } else {
                        value = "'" + value + "'";
                    }
                    listvalueColGap.add(key);
                    listvalueGap.add(value);
                }
                String valNameDailyChange = StringUtils.join(listvalueGap, ",");
                String colNameDailyChange = StringUtils.join(listvalueColGap, ",");

                stmt.addBatch("INSERT INTO [NPT2].[dbo].[" + tableQueryGap + "] (" + colNameDailyChange + ") VALUES (" + valNameDailyChange + ")");

                if (initdata % 10000 == 0) {
                    System.err.println("terisnert = " +tableQueryGap+"="+initdata);
                    stmt.executeBatch();
                    stmt.clearBatch();
                }
                initdata++;
                listvalueColGap.clear();
                listvalueGap.clear();
            }
            try {
                long start = System.currentTimeMillis();
                updateCounts = stmt.executeBatch();
                long end = System.currentTimeMillis();
                System.out.println("total time taken to insert the batch = " + (end - start) + " ms");
                printStatusInsertionStylogap(gagal, dbName, tableName, updateCounts.length, gagal);
                stmt.clearBatch();
                stmt.close();
                returncondition = "sukses";
            } catch (Exception e) {
                System.err.println("List value tabel GAP is null");
            }

        } catch (Exception e) {
            e.printStackTrace();
            returncondition = "gagal";
        }
        return changesData;
    }

    public static ArrayList<HashMap<String, String>> compareData(List<String> listNameColumn, String query, String H_1_Data, ArrayList<HashMap<String, String>> hData, String tabel) throws SQLException {
        ArrayList<HashMap<String, String>> changesData = new ArrayList<>();
//        ArrayList<HashMap<String, String>> hMin1DatatempMySQL = mysqlLoader.load2LastDayDataFromStylo(listNameColumn, query, H_1_Data);
        ArrayList<HashMap<String, String>> hMin1Data=mysqlLoader.load2LastDayDataFromStylo(listNameColumn, query, H_1_Data);
//        if (hMin1DatatempMySQL == null) {
//            hMin1Data = load2LastDayFromStylo(listNameColumn, query, H_1_Data, tabel);
//        } else {
//            hMin1Data = hMin1DatatempMySQL;
//        }
        System.err.println("sukses load data");
        for (Iterator<HashMap<String, String>> iterator = hData.iterator(); iterator.hasNext();) {
            HashMap<String, String> hRow = iterator.next();
            HashMap<String, String> changeRow = new LinkedHashMap();
            String DN = hRow.get("DN");
            String NE_ID = hRow.get("NE_ID");
            String MO_ID = hRow.get("MO_ID");
            String BSC = hRow.get("BSC");
            String CELL = hRow.get("CELL");
            if (DN != null) { // untuk mengecek apakah tabel tersebut ada column DN atau tidak
                for (Iterator<HashMap<String, String>> iterator1 = hMin1Data.iterator(); iterator1.hasNext();) {
                    HashMap<String, String> hMin1Row = iterator1.next();
                    if (hMin1Row.get("DN").equals(DN)) {
                        for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                            String col = colVal.getKey();
                            String val = colVal.getValue();
                            if (val != null && hMin1Row.get(col) != null) {
                                if (val.equals(hMin1Row.get(col)) && !col.equals("DN")) { // saat data h=(h-1)
                                    changeRow.put(col, null);
                                } else if (!col.equals("DN")) { // saat data h!=(h-1)
                                    changeRow.put(col, val);
                                }
                            }
                        }
                    }
                }
                changesData.add(changeRow);
            } else {
                if (NE_ID != null) {
                    for (HashMap<String, String> hMin1Row : hMin1Data) {
                        if (hMin1Row.get("NE_ID").equals(NE_ID)) {
                            for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                                String col = colVal.getKey();
                                String val = colVal.getValue();
                                if (val != null && hMin1Row.get(col) != null) {
                                    if (val.equals(hMin1Row.get(col)) && !col.equals("NE_ID")) { // saat data h=(h-1)
                                        changeRow.put(col, null);
                                    } else if (!col.equals("NE_ID")) { // saat data h!=(h-1)
                                        changeRow.put(col, val);
                                    }
                                }
                            }
                        }
                    }
                    changesData.add(changeRow);
                } else {
                    if (MO_ID != null) {
                        for (Iterator<HashMap<String, String>> iterator1 = hMin1Data.iterator(); iterator1.hasNext();) {
                            HashMap<String, String> hMin1Row = iterator1.next();
                            if (hMin1Row.get("MO_ID").equals(MO_ID)) {
                                for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                                    String col = colVal.getKey();
                                    String val = colVal.getValue();
                                    if (val != null && hMin1Row.get(col) != null) {
                                        if (val.equals(hMin1Row.get(col)) && !col.equals("MO_ID")) { // saat data h=(h-1)
                                            changeRow.put(col, null);
                                        } else if (!col.equals("MO_ID")) { // saat data h!=(h-1)
                                            changeRow.put(col, val);
                                        }
                                    }
                                }
                            }
                        }
                        changesData.add(changeRow);
                    } else {
                        if (BSC != null) {
                            for (Iterator<HashMap<String, String>> iterator1 = hMin1Data.iterator(); iterator1.hasNext();) {
                                HashMap<String, String> hMin1Row = iterator1.next();
                                if (hMin1Row.get("BSC").equals(BSC)) {
                                    for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                                        String col = colVal.getKey();
                                        String val = colVal.getValue();
                                        if (val != null && hMin1Row.get(col) != null) {
                                            if (val.equals(hMin1Row.get(col)) && !col.equals("BSC")) { // saat data h=(h-1)
                                                changeRow.put(col, null);
                                            } else if (!col.equals("BSC")) { // saat data h!=(h-1)
                                                changeRow.put(col, val);
                                            }
                                        }
                                    }
                                }
                            }
                            changesData.add(changeRow);
                        } else {
                            if (CELL != null) {
                                for (Iterator<HashMap<String, String>> iterator1 = hMin1Data.iterator(); iterator1.hasNext();) {
                                    HashMap<String, String> hMin1Row = iterator1.next();
                                    if (hMin1Row.get("CELL").equals(CELL)) {
                                        for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                                            String col = colVal.getKey();
                                            String val = colVal.getValue();
                                            if (val != null && hMin1Row.get(col) != null) {
                                                if (val.equals(hMin1Row.get(col)) && !col.equals("CELL")) { // saat data h=(h-1)
                                                    changeRow.put(col, null);
                                                } else if (!col.equals("CELL")) { // saat data h!=(h-1)
                                                    changeRow.put(col, val);
                                                }
                                            }
                                        }
                                    }
                                }
                                changesData.add(changeRow);
                            }
                        }
                    }
                }
            }
        }
        return changesData;
    }
    
    
    public static ArrayList<HashMap<String, String>> compareDataNPT(List<String> listNameColumn, String query, ArrayList<HashMap<String, String>> hData, String tabel,String NPT_DATE_COUNTER ) throws SQLException {
        ArrayList<HashMap<String, String>> changesData = new ArrayList<>();
        ArrayList<HashMap<String, String>> hMin1Data;
        
        hMin1Data = loadNPTDataFromStylo(listNameColumn,NPT_DATE_COUNTER,tabel);

        for (Iterator<HashMap<String, String>> iterator = hData.iterator(); iterator.hasNext();) {
            HashMap<String, String> hRow = iterator.next();
            HashMap<String, String> changeRow = new LinkedHashMap();
            String DN = hRow.get("DN");
            String NE_ID = hRow.get("NE_ID");
            String MO_ID = hRow.get("MO_ID");
            String BSC = hRow.get("BSC");
            String CELL = hRow.get("CELL");
            if (DN != null) { // untuk mengecek apakah tabel tersebut ada column DN atau tidak
                for (Iterator<HashMap<String, String>> iterator1 = hMin1Data.iterator(); iterator1.hasNext();) {
                    HashMap<String, String> hMin1Row = iterator1.next();
                    if (hMin1Row.get("DN").equals(DN)) {
                        for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                            String col = colVal.getKey();
                            String val = colVal.getValue();
                            if (val != null && hMin1Row.get(col) != null) {
                                if (val.equals(hMin1Row.get(col)) && !col.equals("DN")) { // saat data h=(h-1)
                                    changeRow.put(col, null);
                                } else if (!col.equals("DN")) { // saat data h!=(h-1)
                                    changeRow.put(col, val);
                                }
                            }
                        }
                    }
                }
                changesData.add(changeRow);
            } else {
                if (NE_ID != null) {
                    for (HashMap<String, String> hMin1Row : hMin1Data) {
                        if (hMin1Row.get("NE_ID").equals(NE_ID)) {
                            for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                                String col = colVal.getKey();
                                String val = colVal.getValue();
//                                if (val != null && hMin1Row.get(col) != null) {
                                    if (val==hMin1Row.get(col) && !col.equals("NE_ID")) { // saat data h=(h-1)
                                        changeRow.put(col, null);
                                    } else if (!col.equals("NE_ID")) { // saat data h!=(h-1)
                                        changeRow.put(col, val);
                                    }
//                                }
                            }
                        }
                    }
                    changesData.add(changeRow);
                } else {
                    if (MO_ID != null) {
                        for (Iterator<HashMap<String, String>> iterator1 = hMin1Data.iterator(); iterator1.hasNext();) {
                            HashMap<String, String> hMin1Row = iterator1.next();
                            if (hMin1Row.get("MO_ID").equals(MO_ID)) {
                                for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                                    String col = colVal.getKey();
                                    String val = colVal.getValue();
//                                    if (val != null && hMin1Row.get(col) != null) {
                                        if (val==hMin1Row.get(col) && !col.equals("MO_ID")) { // saat data h=(h-1)
                                            changeRow.put(col, null);
                                        } else if (!col.equals("MO_ID")) { // saat data h!=(h-1)
                                            changeRow.put(col, val);
                                        }
//                                    }
                                }
                            }
                        }
                        changesData.add(changeRow);
                    } else {
                        if (BSC != null) {
                            for (Iterator<HashMap<String, String>> iterator1 = hMin1Data.iterator(); iterator1.hasNext();) {
                                HashMap<String, String> hMin1Row = iterator1.next();
                                if (hMin1Row.get("BSC").equals(BSC)) {
                                    for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                                        String col = colVal.getKey();
                                        String val = colVal.getValue();
//                                        if (val != null && hMin1Row.get(col) != null) {
                                            if (val==hMin1Row.get(col) && !col.equals("BSC")) { // saat data h=(h-1)
                                                changeRow.put(col, null);
                                            } else if (!col.equals("BSC")) { // saat data h!=(h-1)
                                                changeRow.put(col, val);
                                            }
//                                        }
                                    }
                                }
                            }
                            changesData.add(changeRow);
                        } else {
                            if (CELL != null) {
                                for (Iterator<HashMap<String, String>> iterator1 = hMin1Data.iterator(); iterator1.hasNext();) {
                                    HashMap<String, String> hMin1Row = iterator1.next();
                                    if (hMin1Row.get("CELL").equals(CELL)) {
                                        for (Map.Entry<String, String> colVal : hRow.entrySet()) {
                                            String col = colVal.getKey();
                                            String val = colVal.getValue();
//                                            if (val != null && hMin1Row.get(col) != null) {
                                                if (val==hMin1Row.get(col) && !col.equals("CELL")) { // saat data h=(h-1)
                                                    changeRow.put(col, null);
                                                } else if (!col.equals("CELL")) { // saat data h!=(h-1)
                                                    changeRow.put(col, val);
                                                }
//                                            }
                                        }
                                    }
                                }
                                changesData.add(changeRow);
                            }
                        }
                    }
                }
            }
        }
        return changesData;
    }

    public static void generateCsv(String table, ArrayList<HashMap<String, String>> rows, ArrayList<String> resultColumn,String H_Date_yesterday) throws SQLException, IOException {
        ArrayList<String> ListValueCsv = new ArrayList<>();
        ArrayList<String> ListCsv = new ArrayList<>();
        for (Iterator<HashMap<String, String>> iterator = rows.iterator(); iterator.hasNext();) {
            HashMap<String, String> next = iterator.next();
            for (Map.Entry<String, String> entry : next.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value == null) {
                    value = null;
                } else {
                    value = value;
                }
                ListCsv.add(value);
            }
            String resultValuedata = StringUtils.join(ListCsv, ";");
            ListValueCsv.add(resultValuedata);
            ListCsv.clear();
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String[] TanggalCsvsplit = H_Date_yesterday.split("-");
        String TanggalCsv =TanggalCsvsplit[0]+TanggalCsvsplit[1]+TanggalCsvsplit[2];
        String fileName = "D:\\NPT\\NPT\\" + TanggalCsv + "_" + table + ".csv";
//        String fileName = "X:\\NPT\\NPT\\"+TanggalCsv+"_"+table+".csv";
        String outputname = TanggalCsv + "_" + table;
        if (ListValueCsv.isEmpty() || ListValueCsv.equals(null) || ListValueCsv == null) {
            System.err.println("value is null | gagal create csv ");
        } else {
            if (csvWriter.checkFileName(outputname)) {
                if (csvWriter.UpdateCSV(fileName, ListValueCsv).equals("berhasil")) {
                    System.out.println("Csv berhasil di update");
                } else {
                    System.out.println("Csv gagal di update");
                }
            } else {
                if (csvWriter.writeCsvFile(fileName, resultColumn, ListValueCsv).equals("berhasil")) {
                    System.out.println("Csv berhasil di buat");
                }
            }
        }
    }
    
    

    public static String checkAmounColumnTableStylo(ArrayList<String> listNameColumn, String tabelName, String dbName) throws SQLException {
        int amountColumnprev = amountRowSQLServerStylo(dbName + "_" + tabelName);
        int amountColumnCurr = listNameColumn.size();
        if (amountColumnCurr == amountColumnprev) {
            return "equals";
        } else {
            return "notequals";
        }
    }

    public static Integer amountRowSQLServerStylo(String tabel) throws SQLException {
        Statement statement = conectionMsSQL.getConnection().createStatement();
        ResultSet amountColSQL = statement.executeQuery("SELECT COUNT(*) AS jmlhcolumn FROM NPT2.INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '" + tabel + "'");
        int column = 0;
        while (amountColSQL.next()) {
            column = Integer.parseInt(amountColSQL.getString("jmlhcolumn"));
        }
        statement.close();
        return column;
    }

    public static ArrayList<String> getNameColumnPrevStylo(String tabelName, String dbName) {
        ArrayList<String> nameColumn = new ArrayList<String>();
        try {
            Statement statement = conectionMsSQL.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT COLUMN_NAME,COLUMN_TYPE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION "
                    + "FROM INFORMATION_SCHEMA.COLUMNS "
                    + "where table_name='" + tabelName + "' and TABLE_SCHEMA='" + dbName + "'");
            while (resultSet.next()) {
                nameColumn.add(resultSet.getString("COLUMN_NAME"));
            }
            //statement.close();
        } catch (Exception e) {
        }
        return nameColumn;
    }

    public static String AlterTabletoStylo(List<String> typeCol, List<String> listNameColumn, String dbName, String tabelName) throws SQLException {
        ArrayList<String> columnChange = new ArrayList<>();
        ArrayList<String> namaColumnPrev = new ArrayList<>();
        ArrayList<String> columnChangeAlterValue = new ArrayList<>();

        namaColumnPrev = getNameColumnPrevStylo(tabelName, dbName);

        boolean contains = false;
        for (int i = 0; i < namaColumnPrev.size(); i++) {
            for (int j = 0; j < listNameColumn.size(); j++) {
                if (namaColumnPrev.get(i).equals(listNameColumn.get(j))) {
                    contains = true;
                    break;
                }
            }

            if (!contains) {
                columnChange.add(namaColumnPrev.get(i));
            } else {
                contains = false;
            }
        }
        for (int i = 0; i < listNameColumn.size(); i++) {
            for (int j = 0; j < namaColumnPrev.size(); j++) {
                if (listNameColumn.get(i) == namaColumnPrev.get(j)) {
                    contains = true;
                    break;
                }
            }

            if (!contains) {
                columnChange.add(listNameColumn.get(i));
            } else {
                contains = false;
            }
        }

        for (int i = 0; i < columnChange.size(); i++) {
            columnChangeAlterValue.add(columnChange.get(i) + " VARCHAR(255)");
        }

        String columnChangeAlter = StringUtils.join(columnChangeAlterValue, ",");
        try {
            PreparedStatement preparedStatement = conectionMsSQL.getConnection().prepareStatement("ALTER TABLE [NPT2].[dbo].[" + dbName + "_" + tabelName + "] ADD " + columnChangeAlter + "");
            if (preparedStatement.executeUpdate() == 1) {

            } else {

            }
            return "sukses";
        } catch (SQLException e) {
            return "gagal";
        }
    }

}
