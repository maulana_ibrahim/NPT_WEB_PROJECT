/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NPT;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Cell;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Maulana
 */
public class CsvFileWriter {
    private static final String NEW_LINE_SEPARATOR = "\n";

    
    String writeCsvFile(String filename,List<String> columnName,List value){
        FileWriter fileWriter = null;
        String FILE_HEADER = StringUtils.join(columnName,";");
        ArrayList<String> valueinCsv = new ArrayList<String>();
        try{
            fileWriter = new FileWriter(filename,false);
            fileWriter.append(FILE_HEADER.toString());
            fileWriter.append(NEW_LINE_SEPARATOR);
            for(int i=0;i<value.size();i++){
                //String valueCsv = value.get(i).toString().substring(1, value.get(i).toString().length()-2);
                String valueCsv = String.valueOf(value.get(i));
                String[] hasilSplit = valueCsv.split(";");
                for(int a= 0;a<hasilSplit.length;a++){
                    valueinCsv.add(hasilSplit[a].trim());
                }
                valueCsv = StringUtils.join(valueinCsv,";");
                fileWriter.append(valueCsv);
                fileWriter.append(NEW_LINE_SEPARATOR);
                valueinCsv.clear();
            }
           return "berhasil"; 
        } catch (Exception e) {
            Logger.getLogger(CsvFileWriter.class.getName()).log(Level.SEVERE, null, e);
            return "gagal";
        }finally{
            try{
                fileWriter.close();
            }catch(IOException e){
                System.out.println("Error while flushing/closing file writer !!!");
                e.printStackTrace();
            }
        }
    }
    
    
    
    boolean checkFileName(String outputFileName) {
        boolean flag = false;
        File folder = new File("D:\\NPT\\NPT\\");
//        File folder = new File("X:\\NPT\\NPT\\");
        File[] listOfFiles = folder.listFiles();
        String files;
        String name = outputFileName + ".csv";
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                files = listOfFiles[i].getName();
                if (files.equals(name)) {
                    System.out.println("File Exists:::" + files
                            + "and output file name is::::" + outputFileName);
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }
    
    String UpdateCSV(String filename,List value) throws IOException {
        ArrayList<String> valueinCsv = new ArrayList<String>();
        FileWriter fileWriter = null;
        try{
            fileWriter = new FileWriter(filename,true);
            for(int i=0;i<value.size();i++){
                //String valueCsv = value.get(i).toString().substring(1, value.get(i).toString().length()-2);
                String valueCsv = String.valueOf(value.get(i));
                String[] hasilSplit = valueCsv.split(";");
                for(int a= 0;a<hasilSplit.length;a++){
                    valueinCsv.add(hasilSplit[a].trim());
                }
                valueCsv = StringUtils.join(valueinCsv,";");
                fileWriter.append(valueCsv);
                fileWriter.append(NEW_LINE_SEPARATOR);
                valueinCsv.clear();
            }
           return "berhasil"; 
        } catch (Exception e) {
            Logger.getLogger(CsvFileWriter.class.getName()).log(Level.SEVERE, null, e);
            return "gagal";
        }finally{
            try{
                fileWriter.close();
            }catch(IOException e){
                System.out.println("Error while flushing/closing file writer !!!");
                e.printStackTrace();
            }
        }
    }


}
